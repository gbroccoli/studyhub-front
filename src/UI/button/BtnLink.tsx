import { FC } from "react"
import { Link } from "react-router-dom"


interface PropsBtnLink {
    name: string
    link: string
}

const BtnLink:FC<PropsBtnLink> = ({name, link}) => {
    return (
        <button type="button" className="btn btn-main">
            <Link className="block" to={link}>
                {name}
            </Link>
        </button>
    )
}

export default BtnLink