import { FC } from "react"

interface PropsBtn {
    name: string,
    type: "button" | "submit" | "reset",
    func?: () => void
}


const BtnUI: FC<PropsBtn> = ({ name, type, func }) => {

    const handleClick = () => {
        if (func) {
            func()
        }
    }

    return (
        <button type={type} className="btn btn-main" onClick={handleClick}>
            {name}
        </button>
    )
}

export default BtnUI;