import { FC } from "react";
import NavigateItem from "./NavigateItem";

interface NavList {
    name: string,
    link: string,
    clildren?: NavList[]
}

interface HeaderProps {
    navList: NavList[];
}

const Navigate: FC<HeaderProps> = ({ navList }) => {
    return (
        <nav className="flex items-center">
            <ul className="flex space-x-4 font-medium min-lg-desktop">
                {navList.map((nav: NavList, item: number) => (
                    <NavigateItem nav={nav} key={item} />
                ))}
            </ul>
        </nav>
    )
}

export default Navigate;