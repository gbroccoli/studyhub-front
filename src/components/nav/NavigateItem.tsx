import { FC } from "react";
import { Link } from "react-router-dom";
import ArrowDown from "../../UI/icon/ArrowDown";

interface NavList {
    name: string,
    link: string,
    clildren?: NavList[]
}

interface Props {
    nav: NavList
}

const NavigateItem: FC<Props> = ({ nav }) => {
    return (
        <li className="main-item relative flex items-center space-x-1">
            <Link to={nav.link}>
                {nav.name}
            </Link>
            {nav.clildren !== undefined ? <ArrowDown /> : undefined}
            {nav.clildren && (
                <ul className="list-hidden level">
                    {nav.clildren.map((child: NavList, index) => (
                        <li key={index}>
                            <Link to={child.link}>{child.name}</Link>
                        </li>
                    ))}
                </ul>
            )}
        </li>
    )
}

export default NavigateItem;