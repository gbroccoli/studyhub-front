import _ from "lodash";

const Footer = () => {

    const year = new Date(_.now()).getFullYear();

    return (

        <footer className="py-5">
            <div className="container mx-auto flex items-center justify-between">
                <p>© {year} StudyHub – gbroccoli. Alfa test.</p>

                <div>
                    <a className="a_footer_report" href="https://t.me/gbroccoli" target="_blank">
                        Напишите если есть какие-то улучшения или недочеты
                    </a>

                    <span>
                        &nbsp; или &nbsp;
                    </span>

                    <a className="a_footer_report" href="mailto:hei@gbroccoli.ru">
                        напиши мне на почту
                    </a>
                </div>
            </div>
        </footer>
    )
}

export default Footer;