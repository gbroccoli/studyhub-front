import { FC } from "react"
import BtnLink from "../../UI/button/BtnLink"

interface News {
    title: string,
    name: string,
    id: number,
    created_at: Date
}

interface PropsNewsItem {
    item: News,
    index: number
}

const NewsItem: FC<PropsNewsItem> = ({ item, index }) => {
    return (
        <div className="flex flex-col news-item space-y-2" key={index}>
            <h2>{item.title}</h2>
            <div className="h-full">
                {item.name.length > 350 ? <p className="text-sm">{item.name.slice(0, 350)}...</p> : <p>{item.name}</p>}
            </div>
            {item.name.length > 100 ? <BtnLink name="Подробнее" link={`news/${item.id}`} /> : undefined}
        </div>
    )
}

export default NewsItem;