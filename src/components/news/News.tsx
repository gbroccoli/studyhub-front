import { useEffect, useState } from "react"
import $app from "../../app/server"
import NewsItem from "./NewsItem"

interface News {
    title: string,
    name: string,
    id: number,
    created_at: Date
}

const News = () => {

    const [news, setNews] = useState<News[]>([])
    const [err, setErr] = useState<boolean>(false)

    const getList = async () => {
        $app.get('/news/list?limit=5')
            .then((res) => {
                setNews(res.data)
            })
            .catch(() => {
                setErr(true)
            })
    }

    useEffect(() => {
        getList()
    }, [])

    return (
        <>
            {err ? <p className="text-red-600">Произошла ошибка</p> :
                news.map((item, index) => {
                    return (
                        <NewsItem item={item} index={index} key={index} />
                    )
                })
            }
        </>
    )
}

export default News;