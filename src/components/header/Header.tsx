import { Link } from "react-router-dom";
import nav from "../../navigator.json";
import Navigate from "../nav/Navigate";

interface NavList {
    name: string,
    link: string,
    clildren?: NavList[]
}

const Header = () => {

    const navList: NavList[] = nav;

    return (
        <header className="py-5">
            <div className="container mx-auto flex items-center justify-between">
                <Link to={"/"}>
                    <img loading="lazy" width="300px" src="/img/logo.png" alt="logo_site" />
                </Link>
                <Navigate navList={navList} />
            </div>
        </header>
    )
}

export default Header;