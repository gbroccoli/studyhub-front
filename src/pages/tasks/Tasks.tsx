import { Outlet } from "react-router-dom"
import Layout from "../layout/layout"


const Tasks = () => {

    return (
        <Layout>
            <Outlet />
        </Layout>
    )
}

export default Tasks;