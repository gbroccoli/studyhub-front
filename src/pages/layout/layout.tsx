import { FC, ReactNode } from "react";
import Header from "../../components/header/Header";
import Footer from "../../components/footer/Footer";


interface ParamsLayout {
    children: ReactNode,
    style?: string
}

const Layout: FC<ParamsLayout> = ({ children, style }) => {

    return (
        <>
            <Header navList={[
                {
                    name: "Задания", link: "tasks", clildren: [
                        {
                            name: "Семестр 5",
                            link: "tasks/5"
                        }
                    ]
                }
            ]} />
            <div className={`container mx-auto ${style}`}>
                {children}
            </div>
            <Footer />
        </>
    )
}

export default Layout;