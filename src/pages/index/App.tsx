import News from "../../components/news/News";
import Layout from "../layout/layout";



const App = () => {

  return (
    <Layout>
      <div className="flex justify-between items-center">
        <div className="news flex flex-col space-y-4 w-full">
          <h2 className="1 font-bold text-[2rem]">Новости</h2>
          <div className="flex gap-[15px]">
            <News />
          </div>
        </div>
      </div>
    </Layout>
  )
}

export default App;