import BtnLink from "../../UI/button/BtnLink"
import Layout from "../layout/layout"


const Error404 = () => {

    return (
        <Layout style="flex items-center justify-center">
            <div className="flex flex-col space-y-4 items-center" >
                <h1 className="title-1" >
                    404 - Not Found
                </h1>
                
                <BtnLink name="Вернуться на главную" link="/"></BtnLink>
            </div>
        </Layout>
    )
}
export default Error404