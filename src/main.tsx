import React from 'react'
import ReactDOM from 'react-dom/client'
import _ from "lodash"
import App from './pages/index/App.tsx'
import {
  createBrowserRouter,
  RouterProvider
} from "react-router-dom"
import './index.css'
import Error404 from './pages/errors/404.tsx'
import clsx, { ClassValue } from "clsx";
import { twMerge } from "tailwind-merge";
import Tasks from './pages/tasks/Tasks.tsx'
import IndexTask from './pages/tasks/IndexTask.tsx'

export function cn(...inputs: ClassValue[]) {
  return twMerge(clsx(inputs));
}

console.log(new Date(_.now()));



const router = createBrowserRouter([
  {
    path: "/",
    children: [
      {
        path: "/",
        element: <App />,
      },
      {
        path: "tasks",
        element: <Tasks />,
        children: [
          {
            path: "",
            element: <IndexTask />
          },
          {
            path: ":taskId",
            element: "",
            children: [
              {
                path: ":subTaskId",
                element: ""
              }
            ]
          }
        ]
      },
    ]
  },
  {
    path: "*",
    element: <Error404 />
  }
])


ReactDOM.createRoot(document.getElementById('root')!).render(
  <React.StrictMode>
    <RouterProvider router={router} />
  </React.StrictMode>,
)
